#  Default Cape

## P8 Header

| Signal | Control                    | Description |
|--------|----------------------------|-------------|
| P8_1   | n/a                        | GND         |
| P8_2   | n/a                        | GND         |
| P8_3   | MSS GPIO_2[0]              | User LED 0  |
| P8_4   | MSS GPIO_2[1]              | User LED 1  |
| P8_5   | MSS GPIO_2[2]              | User LED 2  |
| P8_6   | MSS GPIO_2[3]              | User LED 3  |
| P8_7   | MSS GPIO_2[4]              | User LED 4  |
| P8_8   | MSS GPIO_2[5]              | User LED 5  |
| P8_9   | MSS GPIO_2[6]              | User LED 6  |
| P8_10  | MSS GPIO_2[7]              | User LED 7  |
| P8_11  | MSS GPIO_2[8]              | User LED 8  |
| P8_12  | MSS GPIO_2[9]              | User LED 9  |
| P8_13  | MSS GPIO_2[10]              | User LED 10 |
| P8_14  | MSS GPIO_2[11]             | User LED 11 |
| P8_15  | MSS GPIO_2[12]             | GPIO        |
| P8_16  | MSS GPIO_2[13]             | GPIO        |
| P8_17  | MSS GPIO_2[14]             | GPIO        |
| P8_18  | MSS GPIO_2[15]             | GPIO        |
| P8_19  | MSS GPIO_2[16]             | GPIO        |
| P8_20  | MSS GPIO_2[17]             | GPIO        |
| P8_21  | MSS GPIO_2[18]             | GPIO        |
| P8_22  | MSS GPIO_2[19]             | GPIO        |
| P8_23  | MSS GPIO_2[20]             | GPIO        |
| P8_24  | MSS GPIO_2[21]             | GPIO        |
| P8_25  | MSS GPIO_2[22]             | GPIO        |
| P8_26  | MSS GPIO_2[23]             | GPIO        |
| P8_27  | MSS GPIO_2[24]             | GPIO        |
| P8_28  | MSS GPIO_2[25]             | GPIO        |
| P8_29  | MSS GPIO_2[26]             | GPIO        |
| P8_30  | MSS GPIO_2[27]             | GPIO        |
| P8_31  | core_gpio[0] @ 0x41100000  | GPIO        |
| P8_32  | core_gpio[1] @ 0x41100000  | GPIO        |
| P8_33  | core_gpio[2] @ 0x41100000  | GPIO        |
| P8_34  | core_gpio[3] @ 0x41100000  | GPIO        |
| P8_35  | core_gpio[4] @ 0x41100000  | GPIO        |
| P8_36  | core_gpio[5] @ 0x41100000  | GPIO        |
| P8_37  | core_gpio[6] @ 0x41100000  | GPIO        |
| P8_38  | core_gpio[7] @ 0x41100000  | GPIO        |
| P8_39  | core_gpio[8] @ 0x41100000  | GPIO        |
| P8_40  | core_gpio[9] @ 0x41100000  | GPIO        |
| P8_41  | core_gpio[10] @ 0x41100000 | GPIO        |
| P8_42  | core_gpio[11] @ 0x41100000 | GPIO        |
| P8_43  | core_gpio[12] @ 0x41100000 | GPIO        |
| P8_44  | core_gpio[13] @ 0x41100000 | GPIO        |
| P8_45  | core_gpio[14] @ 0x41100000 | GPIO        |
| P8_46  | core_gpio[15] @ 0x41100000 | GPIO        |

## P9 Header

| Signal | Control                    | Description |
|--------|----------------------------|-------------|
| P9_1   | n/a                        | GND         |
| P9_2   | n/a                        | GND         |
| P9_3   | n/a                        | VCC 3.3V    |
| P9_4   | n/a                        | VCC 3.3V    |
| P9_5   | n/a                        | VDD 5V      |
| P9_6   | n/a                        | VDD 5V      |
| P9_7   | n/a                        | SYS 5V      |
| P9_8   | n/a                        | SYS 5V      |
| P9_9   | n/a                        | NC          |
| P9_10  | n/a                        | SYS_RSTN    |
| P9_11  | core_gpio[0] @ 0x41200000  | GPIO        |
| P9_12  | core_gpio[1] @ 0x41200000  | GPIO        |
| P9_13  | core_gpio[2] @ 0x41200000  | GPIO        |
| P9_14  | core_gpio[3] @ 0x41200000  | GPIO        |
| P9_15  | core_gpio[4] @ 0x41200000  | GPIO        |
| P9_16  | core_gpio[5] @ 0x41200000  | GPIO        |
| P9_17  | core_gpio[6] @ 0x41200000  | GPIO        |
| P9_18  | core_gpio[7] @ 0x41200000  | GPIO        |
| P9_19  | MSS I2C0                   | I2C0 SCL    |
| P9_20  | MSS I2C0                   | I2C0 SDA    |
| P9_21  | core_gpio[8] @ 0x41200000  | GPIO        |
| P9_22  | core_gpio[9] @ 0x41200000  | GPIO        |
| P9_23  | core_gpio[10] @ 0x41200000 | GPIO        |
| P9_24  | core_gpio[11] @ 0x41200000 | GPIO        |
| P9_25  | core_gpio[12] @ 0x41200000 | GPIO        |
| P9_26  | core_gpio[13] @ 0x41200000 | GPIO        |
| P9_27  | core_gpio[14] @ 0x41200000 | GPIO        |
| P9_28  | core_gpio[15] @ 0x41200000 | GPIO        |
| P9_29  | core_gpio[16] @ 0x41200000 | GPIO        |
| P9_30  | core_gpio[17] @ 0x41200000 | GPIO        |
| P9_31  | core_gpio[18] @ 0x41200000 | GPIO        |
| P9_32  | n/a                        | VDD ADC     |
| P9_33  | n/a                        | ADC input 4 |
| P9_34  | n/a                        | AGND        |
| P9_35  | n/a                        | ADC input 6 |
| P9_36  | n/a                        | ADC input 5 |
| P9_37  | n/a                        | ADC input 2 |
| P9_38  | n/a                        | ADC input 3 |
| P9_39  | n/a                        | ADC input 0 |
| P9_40  | n/a                        | ADC input 1 |
| P9_41  | core_gpio[19] @ 0x41200000 | GPIO        |
| P9_42  | core_gpio[20] @ 0x41200000 | GPIO        |
| P9_43  | n/a                        | GND         |
| P9_44  | n/a                        | GND         |
| P9_45  | n/a                        | GND         |
| P9_46  | n/a                        | GND         |
